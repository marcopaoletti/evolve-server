const readline = require('readline');
const bcrypt = require('bcrypt');
/*eslint no-console: "off"*/

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

function askData() {
  rl.question('insert password: ', password => {
		bcrypt.genSalt((err, salt) => {
			if(err){
				console.log(err);
			}
			bcrypt.hash(password, salt, (err2, hash) => {
				if(err){
					console.log(err2);
				}
				console.log(hash);
				rl.close();
			});
		});
	});
}
askData();