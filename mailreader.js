const MailListener = require('mail-listener-next');
const fs = require('fs');
const ical = require('ical');
const meetingModel = require('./models/model.index').Meetings;
const userModel = require('./models/model.index').Users;


const mailListener = new MailListener({
  username: "evolvemeeting@gmail.com",//"marco.paoletti1985@gmail.com",
  password: "guidomeak",//"marcolux1985a4475820a",
  host: "imap.gmail.com",
  port: 993, // imap port
  tls: true,
  connTimeout: 10000, // Default by node-imap
  authTimeout: 5000, // Default by node-imap,
  debug: console.log, // Or your custom function with only one incoming argument. Default: null
  tlsOptions: { rejectUnauthorized: false },
  mailbox: "INBOX", // mailbox to monitor
  searchFilter: ["UNSEEN"], // the search filter being used after an IDLE notification has been retrieved
  markSeen: true, // all fetched email willbe marked as seen and not fetched next time
  fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
  mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
  attachments: true, // download attachments as they are encountered to the project directory
  attachmentOptions: { directory: "attachments/" } // specify a download directory for attachments
});

mailListener.start(); // start listening

// stop listening
//mailListener.stop();

mailListener.on("server:connected", function(){
  console.log("imapConnected");
});

mailListener.on("server:disconnected", function(){
    console.log("imapDisconnected");
    setTimeout(function () {
        console.log('Trying to establish imap connection again');
        mailListener.restart();
    }, 5 * 1000);
});

mailListener.on("error", function(err){
  console.log(err);
});

mailListener.on("mail", function(mail, seqno, attributes){
  // mail processing code goes here but is out of the scope of the mvp 
});

mailListener.on("attachment", function(attachment){
    if(attachment.contentType === 'application/ics' || attachment.contentType === 'text/calendar') {
        // open the streams
        const output = fs.createWriteStream('invite.ics');
        attachment.stream.pipe(output);
        output.on('finish', () => { 
            let icsData = fs.readFileSync('./invite.ics', 'utf8');
            let ics = ical.parseICS(icsData);
            saveMeeting(ics);
         });
    }
});

function saveMeeting(ics){
    const keys = Object.keys(ics);

    //Prepare the meeting schema
    const meeting = ics[keys[keys.length - 1]];
    if(meeting.summary.val) meeting.summary = meeting.summary.val;

    //Organizer save to db
    let organizer =  {
        name:  meeting.organizer.params.CN,
        email: meeting.organizer.val.split('mailto:')[1],
        password:  meeting.organizer.val.split('mailto:')[1],
        organizer : true,
        role: 'user'
    };

    saveUser(organizer);
    meeting.organizer =  organizer;

    if(meeting.attendee.length){
        let attendee = [];
        for(let myUser of meeting.attendee) {
            if (myUser.params.CN != organizer.name) {
                let user = {
                    name: myUser.params.CN,
                    email: myUser.val.split('mailto:')[1],
                    password: myUser.val.split('mailto:')[1],
                    role: 'user',
                    rated: -1,
                    comment: null
                };
                attendee.push(user);
                saveUser(user);
            }
            meeting.attendee = attendee;
        }
    }
    else{
        let user =  {
            name:  meeting.attendee.params.CN,
            email: meeting.attendee.val.split('mailto:')[1],
            password:  meeting.attendee.val.split('mailto:')[1],
            role: 'user',
            rated : -1,
            comment: null
        };

        saveUser(user);
        meeting.attendee =  [user];
    }

    const meet = new meetingModel(meeting);
    meetingModel.upsert(meet)
    .catch((error) => console.log(error));
}
 
function saveUser(user) {
    return new Promise((resolve, reject) => {
        const newuser = new userModel(user);
        userModel.add(newuser);
    });
}