/*eslint-disable no-unused-vars, no-console*/
const user = require('./models/model.user'); // no MissingSchemaError: Schema hasn't been registered for model "User".
/*eslint-able no-unused-vars*/
const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const helmet = require('helmet');
const compression = require('compression'); // compression middleware
const config = require('./config');
const passport = require('passport');
const passport_options = require('./routes/passport');
const cors = require('cors');

// TODO change this to something not hardcoded
const db_uri = 'mongodb://localhost/evolve';
mongoose.Promise = global.Promise;

mongoose.connect(db_uri);
mongoose.connection.on('connected', () => {
  console.log(`Mongoose default connection open to ${db_uri}`);
});

// If the connection throws an error
mongoose.connection.on('error', () => {
  console.log(`Mongoose default connection error: ${db_uri}`);
});


const app = express();
app.use(passport.initialize());

passport.use('local-signup', passport_options.signup);
passport.use('local-login', passport_options.login);
passport.use('token', passport_options.token);
passport.serializeUser(passport_options.serializeUser);

// security best practices: https://expressjs.com/en/advanced/best-practice-security.html
app.set('trust proxy', 'loopback');
app.use(helmet()); // default modules only
app.use(helmet.noCache()); // module for disabling client-side caching
app.use(compression({
  threshold: 1024
}));
// CORS middleware
app.use(cors());

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('./routes/routes.index'));
// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;