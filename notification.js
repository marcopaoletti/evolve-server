/**
 * Created by marcellomorra on 18/04/18.
 */

const schedule = require('node-schedule');
const meeting = require('./models/model.meeting');
const user = require('./models/model.user');
const async = require('async');
const PushNotifications = require('node-pushnotifications');

const settings = {
    apn: {
        token: {
            key: './certs/AuthKey_AQRGKGNDMS.p8',
            keyId: 'AQRGKGNDMS',
            teamId: 'A2J6CUCJD3',
            production: true // true for APN production environment, false for APN sandbox environment
        },
        production: true // true for APN production environment, false for APN sandbox environment

    },
    gcm: {
        id: 'AIzaSyBnIF4yqBP7Nb6e5fX9P7eidP2HsXEWgEY',
        phonegap: false, // phonegap compatibility mode, see below (defaults to false)
     },
};
const push = new PushNotifications(settings);

const data = {
    title: 'Rate a new meeting', // REQUIRED
    body: 'Evolve App', // REQUIRED
    topic: 'com.nembol.evolve', // REQUIRED for apn and gcm for ios
    custom: {
        sender: 'evolve meetings',
    },
    priority: 'high', // gcm, apn. Supported values are 'high' or 'normal' (gcm). Will be translated to 10 and 5 for apn. Defaults to 'high'
    collapseKey: '', // gcm for android, used as collapseId in apn
    contentAvailable: true, // gcm, apn. node-apn will translate true to 1 as required by apn.
    delayWhileIdle: true, // gcm for android
    restrictedPackageName: '', // gcm for android
    dryRun: false, // gcm for android
    icon: '', // gcm for android
    tag: '', // gcm for android
    color: '', // gcm for android
    clickAction: '', // gcm for android. In ios, category will be used if not supplied
    locKey: '', // gcm, apn
    bodyLocArgs: '', // gcm, apn
    retries: 1, // gcm, apn
    encoding: '', // apn
    badge: 0, // gcm for ios, apn
    sound: 'ping.aiff', // gcm, apn
    alert: 'Rate a new meeting', // apn, will take precedence over title and body
    // alert: '', // It is also accepted a text message in alert
    titleLocKey: '', // apn and gcm for ios
    titleLocArgs: [], // apn and gcm for ios
    launchImage: '', // apn and gcm for ios
    action: '', // apn and gcm for ios
    category: '', // apn and gcm for ios
    //mdm: '', // apn and gcm for ios
    urlArgs: '', // apn and gcm for ios
    truncateAtWordEnd: true, // apn and gcm for ios
    mutableContent: 0, // apn
    threadId: '', // apn
    expiry: Math.floor(Date.now() / 1000) + 28 * 86400, // seconds
    timeToLive: 28 * 86400, // if both expiry and timeToLive are given, expiry will take precedency
    headers: [], // wns
    launch: '', // wns
    duration: '', // wns
    consolidationKey: 'my notification', // ADM
};

const rule_push_notification = new schedule.RecurrenceRule();

rule_push_notification.second = 10;

const temp = schedule.scheduleJob(rule_push_notification, pushNotification.bind(null, err => {
    if (err) {
        console.log(`Error pushNotification ${err}`)
    } else {
        console.log(`Done pushNotification ${new Date()}`);
    }
}));


function pushNotification(callback) {

    const date =  new Date().getTime();
    const options = {
        "status" : "CONFIRMED",
        "end": {$lt: date},
        "notification": false
    };
    meeting.getByOption(options)
        .then(meetings => {
            async.forEachOf(meetings, (value, key, callback) => {
                const filter = {
                    "_id": value._id,
                };
                const update = {
                    $set: {
                        "notification": true,
                    }
                };
                meeting.setNotificationSend(filter, update)
                    .then(results => {
                        async.forEachOf(value.attendee, (attendee, key, callback) => {
                            const option = {
                                "email": attendee.email,
                            };
                            user.getByOption(option)
                                .then(res => {
                                    if(res) {
                                        if(res[0]) {
                                            if (res[0].notificationToken) {
                                                push.send([res[0].notificationToken], data, (err, result) => {
                                                    if (err) {
                                                        console.dir(err, {depth:null});
                                                        return callback(err);
                                                    } else {
                                                        console.dir(result, {depth:null});
                                                        callback();
                                                    }
                                                });
                                            }
                                            else{
                                                callback();
                                            }
                                        }
                                        else{
                                            callback();
                                        }
                                    }
                                    else {
                                        callback();
                                    }
                                }).catch(err => {
                                    callback(err);
                            });
                        }, err => {
                            callback();
                            if (err) console.error(err);
                        });
                    }).catch(err => {
                        callback(err);
                });
            }, err => {
                callback();
                if (err) console.error(err);
            });
        }).catch(err => {
        console.log("index.js~get/option~error", err);
    });
}

