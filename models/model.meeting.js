const mongoose   = require('mongoose');
const timestamps = require('mongoose-timestamp');
const User       = require('./model.user.js');

let MeetingSchema = new mongoose.Schema({
  type: {
    type: String,
    trim: true,
  },
  uid: {
    type: String,
    trim: true,
  },
  summary: {
    type: String,
    trim: true,
  },
  status: {
    type: String,
    trim: true,
    default: 'CONFIRMED',
  },
  location: {
    type: String,
    trim: true,
  },
  description: {
    type: String,
    trim: true,
  },
  start:{
    type: Date,
  },
  end: {
    type: Date,
  },
  url: {
    type: String,
    trim: true,
  },
  notification: {
       type: Boolean,
       default: false,
  },
  attendee : [{
          name: {
              type: String,
              trim: true
          },
          email: {
              type: String,
              trim: true
          },
          role: {
              type: String,
              trim: true
          },
          rated: {
              type: Number,
              trim: true,
              default: -1,
          },
         comment: {
             type: String,
             trim: true
         },
          rating_areas: {
              type: Array,
              trim: true,
              default: []
          },
  }],
  organizer: {
      name: {
          type: String,
          trim: true
      },
      email: {
          type: String,
          trim: true
      },
      organizer :{
          type: Boolean
      },
      role: {
          type: String,
          trim: true
      },
  }
}, {
  collection : 'meetings'
});

MeetingSchema.plugin(timestamps, {
  createdAt: 'created_at',
  updatedAt: 'updated_at',
});


/**************FUNCTIONS*****************/

MeetingSchema.statics.getByOption = function(options) {
  return new Promise((resolve, reject) => {
    this.find(options).sort({
      updated_at: 1
    }).lean().exec((error, results) => {
        if(error){
            return reject(error);
        }
        else{
            return resolve(results)
        }
    });
  });
};

MeetingSchema.statics.getAll = function(options) {
    return new Promise((resolve, reject) => {
        this.find(options).sort({
            'organizer.name' : -1
        }).lean().exec((error, results) => {
            if(error){
                return reject(error);
            }
            else{
                return resolve(results)
            }
        });
    });
};

MeetingSchema.statics.putVote = function(filter, update) {
    return new Promise((resolve, reject) => {
        this.findOneAndUpdate(filter, update)
            .lean().exec((error, results) => {
            if(error){
                return reject(error);
            }
            else{
                return resolve(results)
            }
        });
    });
};

MeetingSchema.statics.setNotificationSend = function(filter, update) {
    return new Promise((resolve, reject) => {
        this.findOneAndUpdate(filter, update)
            .lean().exec((error, results) => {
            if(error){
                return reject(error);
            }
            else{
                return resolve(results)
            }
        });
    });
};

MeetingSchema.statics.upsert = function (meeting) {

    let meetingToUpsert = {};

    Object.assign(meetingToUpsert, meeting._doc);

    delete meetingToUpsert._id;
    const {
        type,
        uid,
        summary,
        status,
        location,
        description,
        url,
        start,
        end
    } = meeting._doc;
    const options = {
      upsert: true,
        returnOriginal: true
    };
    const filter = {
      uid : meetingToUpsert.uid
    };
    return new Promise((resolve, reject) => {
        this.findOneAndUpdate(filter, {
            $set: meetingToUpsert
        }, options, (err, doc) => {
            if(err) return reject(err);
            return resolve(meetingToUpsert);
        });
    });
}


const meeting = mongoose.model('Meeting', MeetingSchema);
module.exports = meeting;
