const mongoose   = require('mongoose');
const timestamps = require('mongoose-timestamp');
const bcrypt = require('bcrypt');

let UserSchema = new mongoose.Schema({
   email: {
     type: String,
     index: {
       unique: true,
     },
   },
   password: String,
   name: String,
   role: {
     type: String,
     required: true,
     enum: ['super', 'user', 'admin'],
   },
   notificationToken: {
       type: String,
   },
   company: {
     type: String
   },
 }, {
 	  collection : 'users'
 });


UserSchema.plugin(timestamps, {
  createdAt: 'created_at',
  updatedAt: 'updated_at',
});

UserSchema.methods.comparePassword = function (password, callback) {
  bcrypt.compare(password, this.password, callback);
};

UserSchema.statics.add = function(user) {
  
  var userToUpsert = {};
  Object.assign(userToUpsert, user._doc);
  delete userToUpsert._id;
  const {
      email,
      name,
      password
  } = user._doc;
  const options = {
    upsert: true,
      returnOriginal: true
  };
  const filter = {
    email : userToUpsert.email
  };

    bcrypt.genSalt((saltError, salt) => {

        bcrypt.hash(user.password, salt, (hashError, hash) => {

            // replace a password string with hash value
            userToUpsert.password = hash;
            return new Promise((resolve, reject) => {
                this.findOne(filter, (err, myDoc) => {
                    if(myDoc.role == 'admin'){
                        userToUpsert.role = 'admin';
                        userToUpsert.name = myDoc.name;
                    }
                    this.findOneAndUpdate(filter, {
                        $set: userToUpsert
                    }, options, (err, doc) => {
                        if (err) return reject(err);
                        return resolve(userToUpsert);
                    });
                });

            });

        });
    });
};

UserSchema.statics.getByOption = function(options) {
    return new Promise((resolve, reject) => {
        this.find(options).sort({
            updated_at: 1
        }).lean().exec((error, results) => {
            if(error){
                return reject(error);
            }
            else{
                return resolve(results)
            }
        });
    });
};

UserSchema.statics.putNotificationToken = function(filter, update) {
    return new Promise((resolve, reject) => {
        this.findOneAndUpdate(filter, update)
            .lean().exec((error, results) => {
            if(error){
                return reject(error);
            }
            else{
                return resolve(results)
            }
        });
    });
};

const generatePassword = function(next) {
    const user = this;

    return bcrypt.genSalt((saltError, salt) => {
      if (saltError) {
        return next(saltError);
      }

      return bcrypt.hash(user.password, salt, (hashError, hash) => {
        if (hashError) {
          return next(hashError);
        }

        // replace a password string with hash value
        user.password = hash;

        return next();
      });
    });
};

/**
 * The pre-save and findAnUpdate hook method.
 */
UserSchema.pre('save', generatePassword);

const user = mongoose.model('User', UserSchema);
module.exports = user;
