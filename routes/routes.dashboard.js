const express = require('express');
const meeting = require('../models/model.meeting');
const _     = require('underscore');
const router = new express.Router();

router.get('/', (req, res) => {
    const date = req.query.date;
    const options = {
        "end": {$lt: date},
        "status" : "CONFIRMED",
    };
    let allMeeting = {
        meetingNum: 0,
        averageVote: "",
        averageDuration: 0.00,
        maxRatingPercent : '',
        excellentPercent: 0,
        goodPercent: 0,
        averagePercent: 0,
        poorPercent: 0,
        wastePercent: 0,
        people: 0,
        uniquePeople:[],
        peopleVoting: 0,
        totalFeedback: 0,
        agendaDesign : 0,
        meetingcontent : 0,
        facilitator : 0,
        achievingTargets : 0,
        relevanceMeeting : 0,
        teamwork : 0,
        importanceMeeting : 0,
        company : "Hewlett-Packaerd",
        name : req.user.name,
        role : req.user.role,
        managers : [],
        pie : {
            agendaDesignPie : {
                excellent: 0,
                good: 0,
                average: 0,
                poor: 0,
                waste: 0,
            },
            meetingcontentPie : {
                excellent: 0,
                good: 0,
                average: 0,
                poor: 0,
                waste: 0,
            },
            facilitatorPie : {
                excellent: 0,
                good: 0,
                average: 0,
                poor: 0,
                waste: 0,
            },
            achievingTargetsPie : {
                excellent: 0,
                good: 0,
                average: 0,
                poor: 0,
                waste: 0,
            },
            relevanceMeetingPie : {
                excellent: 0,
                good: 0,
                average: 0,
                poor: 0,
                waste: 0,
            },
            teamworkPie : {
                excellent: 0,
                good: 0,
                average: 0,
                poor: 0,
                waste: 0,
            },
            importanceMeetingPie : {
                excellent: 0,
                good: 0,
                average: 0,
                poor: 0,
                waste: 0,
            },
        },
    };
    let manager = {};

    let pieVote = '';

    meeting.getAll(options)
        .then(results => {
            if (results.length){
                allMeeting.meetingNum = results.length;
            /* ************************************ SET MANAGER ****************************************************/
            for (let k = 0; k < results.length; k++) {
                if (manager.name != results[k].organizer.name) {
                    if (manager.name) {
                        const temp = Math.max(manager.wastePercent, manager.poorPercent, manager.averagePercent, manager.goodPercent, manager.excellentPercent);
                        if (temp == manager.wastePercent) {
                            manager.averageVote = 'waste';
                            manager.maxRatingPercent = ((manager.wastePercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        }
                        if (temp == manager.poorPercent) {
                            manager.averageVote = 'poor';
                            manager.maxRatingPercent = ((manager.poorPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        }
                        if (temp == manager.averagePercent) {
                            manager.averageVote = 'average';
                            manager.maxRatingPercent = ((manager.averagePercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        }
                        if (temp == manager.goodPercent) {
                            manager.averageVote = 'good';
                            manager.maxRatingPercent = ((manager.goodPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        }
                        if (temp == manager.excellentPercent) {
                            manager.averageVote = 'excellent';
                            manager.maxRatingPercent = ((manager.excellentPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        }

                        if (temp == 0) manager.averageVote = '';
                        if (manager.maxRatingPercent == 'NaN') manager.maxRatingPercent = '0';

                        manager.wastePercent = ((manager.wastePercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        if (manager.wastePercent == 'NaN') manager.wastePercent = '0';
                        manager.poorPercent = ((manager.poorPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        if (manager.poorPercent == 'NaN') manager.poorPercent = '0';
                        manager.averagePercent = ((manager.averagePercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        if (manager.averagePercent == 'NaN') manager.averagePercent = '0';
                        manager.goodPercent = ((manager.goodPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        if (manager.goodPercent == 'NaN') manager.goodPercent = '0';
                        manager.excellentPercent = ((manager.excellentPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                        if (manager.excellentPercent == 'NaN') manager.excellentPercent = '0';

                        allMeeting.averageDuration = (Number(allMeeting.averageDuration) + Number(manager.averageDuration));
                        manager.averageDuration = (manager.averageDuration / manager.meetingNum).toFixed(0);

                        manager.peopleVoting = ((manager.peopleVoting / manager.people) * 100).toFixed(0).toString();
                        if (manager.peopleVoting == 'NaN') manager.peopleVoting = '0';

                        manager.uniquePeople = _.uniq(manager.uniquePeople);

                        manager.agendaDesign = ((manager.agendaDesign * 100)/manager.totalFeedback).toFixed(0).toString();
                        if (manager.agendaDesign == 'NaN') manager.agendaDesign = '0';
                        manager.meetingcontent = ((manager.meetingcontent * 100)/manager.totalFeedback).toFixed(0).toString();
                        if (manager.meetingcontent == 'NaN') manager.meetingcontent = '0';
                        manager.facilitator = ((manager.facilitator * 100)/manager.totalFeedback).toFixed(0).toString();
                        if (manager.facilitator == 'NaN') manager.facilitator = '0';
                        manager.achievingTargets = ((manager.achievingTargets * 100)/manager.totalFeedback).toFixed(0).toString();
                        if (manager.achievingTargets == 'NaN') manager.achievingTargets = '0';
                        manager.relevanceMeeting = ((manager.relevanceMeeting * 100)/manager.totalFeedback).toFixed(0).toString();
                        if (manager.relevanceMeeting == 'NaN') manager.relevanceMeeting = '0';
                        manager.teamwork = ((manager.teamwork * 100)/manager.totalFeedback).toFixed(0).toString();
                        if (manager.teamwork == 'NaN') manager.teamwork = '0';
                        manager.importanceMeeting = ((manager.importanceMeeting * 100)/manager.totalFeedback).toFixed(0).toString();
                        if (manager.importanceMeeting == 'NaN') manager.importanceMeeting = '0';


                        /* ************************************ SET MANAGER PIE ****************************************************/

                        const agendapieTot = manager.pie.agendaDesignPie.excellent + manager.pie.agendaDesignPie.good + manager.pie.agendaDesignPie.average + manager.pie.agendaDesignPie.poor + manager.pie.agendaDesignPie.waste;
                        if(agendapieTot > 0) {
                            manager.pie.agendaDesignPie.excellent = ((manager.pie.agendaDesignPie.excellent / agendapieTot) * 100).toFixed(2);
                            manager.pie.agendaDesignPie.good = ((manager.pie.agendaDesignPie.good / agendapieTot) * 100).toFixed(2);
                            manager.pie.agendaDesignPie.average = ((manager.pie.agendaDesignPie.average / agendapieTot) * 100).toFixed(2);
                            manager.pie.agendaDesignPie.poor = ((manager.pie.agendaDesignPie.poor / agendapieTot) * 100).toFixed(2);
                            manager.pie.agendaDesignPie.waste = ((manager.pie.agendaDesignPie.waste / agendapieTot) * 100).toFixed(2);
                        }

                        const facilitatorPieTot = manager.pie.facilitatorPie.excellent + manager.pie.facilitatorPie.good + manager.pie.facilitatorPie.average + manager.pie.facilitatorPie.poor + manager.pie.facilitatorPie.waste;
                        if(facilitatorPieTot > 0) {
                            manager.pie.facilitatorPie.excellent = ((manager.pie.facilitatorPie.excellent / facilitatorPieTot) * 100).toFixed(2);
                            manager.pie.facilitatorPie.good = ((manager.pie.facilitatorPie.good / facilitatorPieTot) * 100).toFixed(2);
                            manager.pie.facilitatorPie.average = ((manager.pie.facilitatorPie.average / facilitatorPieTot) * 100).toFixed(2);
                            manager.pie.facilitatorPie.poor = ((manager.pie.facilitatorPie.poor / facilitatorPieTot) * 100).toFixed(2);
                            manager.pie.facilitatorPie.waste = ((manager.pie.facilitatorPie.waste / facilitatorPieTot) * 100).toFixed(2);
                        }

                        const meetingcontentPieTot = manager.pie.meetingcontentPie.excellent + manager.pie.meetingcontentPie.good + manager.pie.meetingcontentPie.average + manager.pie.meetingcontentPie.poor + manager.pie.meetingcontentPie.waste;
                        if(meetingcontentPieTot > 0) {
                            manager.pie.meetingcontentPie.excellent = ((manager.pie.meetingcontentPie.excellent / meetingcontentPieTot) * 100).toFixed(2);
                            manager.pie.meetingcontentPie.good = ((manager.pie.meetingcontentPie.good / meetingcontentPieTot) * 100).toFixed(2);
                            manager.pie.meetingcontentPie.average = ((manager.pie.meetingcontentPie.average / meetingcontentPieTot) * 100).toFixed(2);
                            manager.pie.meetingcontentPie.poor = ((manager.pie.meetingcontentPie.poor / meetingcontentPieTot) * 100).toFixed(2);
                            manager.pie.meetingcontentPie.waste = ((manager.pie.meetingcontentPie.waste / meetingcontentPieTot) * 100).toFixed(2);
                        }

                        const achievingTargetsPieTot = manager.pie.achievingTargetsPie.excellent + manager.pie.achievingTargetsPie.good + manager.pie.achievingTargetsPie.average + manager.pie.achievingTargetsPie.poor + manager.pie.achievingTargetsPie.waste;
                        if(achievingTargetsPieTot > 0) {
                            manager.pie.achievingTargetsPie.excellent = ((manager.pie.achievingTargetsPie.excellent / achievingTargetsPieTot) * 100).toFixed(2);
                            manager.pie.achievingTargetsPie.good = ((manager.pie.achievingTargetsPie.good / achievingTargetsPieTot) * 100).toFixed(2);
                            manager.pie.achievingTargetsPie.average = ((manager.pie.achievingTargetsPie.average / achievingTargetsPieTot) * 100).toFixed(2);
                            manager.pie.achievingTargetsPie.poor = ((manager.pie.achievingTargetsPie.poor / achievingTargetsPieTot) * 100).toFixed(2);
                            manager.pie.achievingTargetsPie.waste = ((manager.pie.achievingTargetsPie.waste / achievingTargetsPieTot) * 100).toFixed(2);
                        }

                        const relevanceMeetingPieTot = manager.pie.relevanceMeetingPie.excellent + manager.pie.relevanceMeetingPie.good + manager.pie.relevanceMeetingPie.average + manager.pie.relevanceMeetingPie.poor + manager.pie.relevanceMeetingPie.waste;
                        if(relevanceMeetingPieTot > 0) {
                            manager.pie.relevanceMeetingPie.excellent = ((manager.pie.relevanceMeetingPie.excellent / relevanceMeetingPieTot) * 100).toFixed(2);
                            manager.pie.relevanceMeetingPie.good = ((manager.pie.relevanceMeetingPie.good / relevanceMeetingPieTot) * 100).toFixed(2);
                            manager.pie.relevanceMeetingPie.average = ((manager.pie.relevanceMeetingPie.average / relevanceMeetingPieTot) * 100).toFixed(2);
                            manager.pie.relevanceMeetingPie.poor = ((manager.pie.relevanceMeetingPie.poor / relevanceMeetingPieTot) * 100).toFixed(2);
                            manager.pie.relevanceMeetingPie.waste = ((manager.pie.relevanceMeetingPie.waste / relevanceMeetingPieTot) * 100).toFixed(2);
                        }

                        const teamworkPieTot = manager.pie.teamworkPie.excellent + manager.pie.teamworkPie.good + manager.pie.teamworkPie.average + manager.pie.teamworkPie.poor + manager.pie.teamworkPie.waste;
                        if(teamworkPieTot > 0) {
                            manager.pie.teamworkPie.excellent = ((manager.pie.teamworkPie.excellent / teamworkPieTot) * 100).toFixed(2);
                            manager.pie.teamworkPie.good = ((manager.pie.teamworkPie.good / teamworkPieTot) * 100).toFixed(2);
                            manager.pie.teamworkPie.average = ((manager.pie.teamworkPie.average / teamworkPieTot) * 100).toFixed(2);
                            manager.pie.teamworkPie.poor = ((manager.pie.teamworkPie.poor / teamworkPieTot) * 100).toFixed(2);
                            manager.pie.teamworkPie.waste = ((manager.pie.teamworkPie.waste / teamworkPieTot) * 100).toFixed(2);
                        }

                        const importanceMeetingPieTot = manager.pie.importanceMeetingPie.excellent + manager.pie.importanceMeetingPie.good + manager.pie.importanceMeetingPie.average + manager.pie.importanceMeetingPie.poor + manager.pie.importanceMeetingPie.waste;
                        if(importanceMeetingPieTot > 0) {
                            manager.pie.importanceMeetingPie.excellent = ((manager.pie.importanceMeetingPie.excellent / importanceMeetingPieTot) * 100).toFixed(2);
                            manager.pie.importanceMeetingPie.good = ((manager.pie.importanceMeetingPie.good / importanceMeetingPieTot) * 100).toFixed(2);
                            manager.pie.importanceMeetingPie.average = ((manager.pie.importanceMeetingPie.average / importanceMeetingPieTot) * 100).toFixed(2);
                            manager.pie.importanceMeetingPie.poor = ((manager.pie.importanceMeetingPie.poor / importanceMeetingPieTot) * 100).toFixed(2);
                            manager.pie.importanceMeetingPie.waste = ((manager.pie.importanceMeetingPie.waste / importanceMeetingPieTot) * 100).toFixed(2);
                        }

                        allMeeting.managers.push(manager);
                        manager = {};
                    }
                    manager.name = results[k].organizer.name;
                    manager.email = results[k].organizer.email;
                    manager.people = results[k].attendee.length -1;
                    manager.uniquePeople = [];
                    allMeeting.people = allMeeting.people + results[k].attendee.length -1;
                    manager.meetingNum = 1;
                    manager.averageDuration = ((results[k].end - results[k].start) / 60000);
                    manager.averageVote = '';
                    manager.maxRatingPercent = '';
                    manager.excellentPercent = 0;
                    manager.goodPercent = 0;
                    manager.averagePercent = 0;
                    manager.poorPercent = 0;
                    manager.wastePercent = 0;
                    manager.peopleVoting = 0;
                    manager.agendaDesign = 0;
                    manager.meetingcontent = 0;
                    manager.facilitator = 0;
                    manager.achievingTargets = 0;
                    manager.relevanceMeeting = 0;
                    manager.teamwork = 0;
                    manager.importanceMeeting = 0;
                    manager.totalFeedback = 0;

                    let myPie = {
                            agendaDesignPie : {
                                excellent: 0,
                                good: 0,
                                average: 0,
                                poor: 0,
                                waste: 0,
                            },
                            meetingcontentPie : {
                                excellent: 0,
                                good: 0,
                                average: 0,
                                poor: 0,
                                waste: 0,
                            },
                            facilitatorPie : {
                                excellent: 0,
                                good: 0,
                                average: 0,
                                poor: 0,
                                waste: 0,
                            },
                            achievingTargetsPie : {
                                excellent: 0,
                                good: 0,
                                average: 0,
                                poor: 0,
                                waste: 0,
                            },
                            relevanceMeetingPie : {
                                excellent: 0,
                                good: 0,
                                average: 0,
                                poor: 0,
                                waste: 0,
                            },
                            teamworkPie : {
                                excellent: 0,
                                good: 0,
                                average: 0,
                                poor: 0,
                                waste: 0,
                            },
                            importanceMeetingPie : {
                                excellent: 0,
                                good: 0,
                                average: 0,
                                poor: 0,
                                waste: 0,
                            },
                        };

                    manager.pie = myPie;
                }
                else {
                    manager.people = manager.people + results[k].attendee.length -1;
                    allMeeting.people = allMeeting.people + results[k].attendee.length -1;
                    manager.averageDuration = Number(manager.averageDuration) + Number(((results[k].end - results[k].start) / 60000));
                    manager.meetingNum++;
                }

                for (let attendee of results[k].attendee) {
                    if(attendee.email != 'evolvemeeting@gmail.com') {
                        allMeeting.uniquePeople.push(attendee.email);
                        manager.uniquePeople.push(attendee.email);
                        if (attendee.rated != -1) {
                            manager.peopleVoting++;
                            allMeeting.peopleVoting++;
                            if (attendee.rated == 1) {
                                manager.wastePercent++;
                                allMeeting.wastePercent++;
                                pieVote = 'waste';
                            }
                            if (attendee.rated == 2) {
                                manager.poorPercent++;
                                allMeeting.poorPercent++;
                                pieVote = 'poor';
                            }
                            if (attendee.rated == 3) {
                                manager.averagePercent++;
                                allMeeting.averagePercent++;
                                pieVote = 'average';
                            }
                            if (attendee.rated == 4) {
                                manager.goodPercent++;
                                allMeeting.goodPercent++;
                                pieVote = 'good';
                            }
                            if (attendee.rated == 5) {
                                manager.excellentPercent++;
                                allMeeting.excellentPercent++;
                                pieVote = 'excellent';
                            }
                        }
        /* ************************************ SET SPECIFIC FEEDBACK ****************************************************/

                        if(attendee.rating_areas.length > 0){
                            for(let feedback of attendee.rating_areas){
                                if(feedback == 'agendaDesign'){
                                    allMeeting.agendaDesign++;
                                    allMeeting.totalFeedback++;

                                    manager.agendaDesign++;
                                    manager.totalFeedback++;

                                    allMeeting.pie.agendaDesignPie[pieVote]++;
                                }
                                else if(feedback == 'meetingcontent'){
                                    allMeeting.meetingcontent++;
                                    allMeeting.totalFeedback++;

                                    manager.meetingcontent++;
                                    manager.totalFeedback++;

                                    allMeeting.pie.meetingcontentPie[pieVote]++;
                                    manager.pie.meetingcontentPie[pieVote]++;
                                }
                                else if(feedback == 'facilitator'){
                                    allMeeting.facilitator++;
                                    allMeeting.totalFeedback++;

                                    manager.facilitator++;
                                    manager.totalFeedback++;

                                    allMeeting.pie.facilitatorPie[pieVote]++;
                                    manager.pie.facilitatorPie[pieVote]++;
                                }
                                else if(feedback == 'achievingTargets'){
                                    allMeeting.achievingTargets++;
                                    allMeeting.totalFeedback++;

                                    manager.achievingTargets++;
                                    manager.totalFeedback++;

                                    allMeeting.pie.achievingTargetsPie[pieVote]++;
                                    manager.pie.achievingTargetsPie[pieVote]++;
                                }
                                else if(feedback == 'relevanceMeeting'){
                                    allMeeting.relevanceMeeting++;
                                    allMeeting.totalFeedback++;

                                    manager.relevanceMeeting++;
                                    manager.totalFeedback++;

                                    allMeeting.pie.relevanceMeetingPie[pieVote]++;
                                    manager.pie.relevanceMeetingPie[pieVote]++;
                                }
                                else if(feedback == 'teamwork'){
                                    allMeeting.teamwork++;
                                    allMeeting.totalFeedback++;

                                    manager.teamwork++;
                                    manager.totalFeedback++;

                                    allMeeting.pie.teamworkPie[pieVote]++;
                                    manager.pie.teamworkPie[pieVote]++;
                                }
                                else if(feedback == 'importanceMeeting'){
                                    allMeeting.importanceMeeting++;
                                    allMeeting.totalFeedback++;

                                    manager.importanceMeeting++;
                                    manager.totalFeedback++;

                                    allMeeting.pie.importanceMeetingPie[pieVote]++;
                                    manager.pie.importanceMeetingPie[pieVote]++;
                                }
                            }
                        }
                    }
                }

                if (k == results.length - 1) {
                    const temp = Math.max(manager.wastePercent, manager.poorPercent, manager.averagePercent, manager.goodPercent, manager.excellentPercent);
                    if (temp == manager.wastePercent) {
                        manager.averageVote = 'waste';
                        manager.maxRatingPercent = ((manager.wastePercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    }
                    if (temp == manager.poorPercent) {
                        manager.averageVote = 'poor';
                        manager.maxRatingPercent = ((manager.poorPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    }
                    if (temp == manager.averagePercent) {
                        manager.averageVote = 'average';
                        manager.maxRatingPercent = ((manager.averagePercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    }
                    if (temp == manager.goodPercent) {
                        manager.averageVote = 'good';
                        manager.maxRatingPercent = ((manager.goodPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    }
                    if (temp == manager.excellentPercent) {
                        manager.averageVote = 'excellent';
                        manager.maxRatingPercent = ((manager.excellentPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    }

                    if (temp == 0) manager.averageVote = '';
                    if (manager.maxRatingPercent == 'NaN') manager.maxRatingPercent = '0';

                    manager.wastePercent = ((manager.wastePercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    if (manager.wastePercent == 'NaN') manager.wastePercent = '0';
                    manager.poorPercent = ((manager.poorPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    if (manager.poorPercent == 'NaN') manager.poorPercent = '0';
                    manager.averagePercent = ((manager.averagePercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    if (manager.averagePercent == 'NaN') manager.averagePercent = '0';
                    manager.goodPercent = ((manager.goodPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    if (manager.goodPercent == 'NaN') manager.goodPercent = '0';
                    manager.excellentPercent = ((manager.excellentPercent / manager.peopleVoting) * 100).toFixed(0).toString();
                    if (manager.excellentPercent == 'NaN') manager.excellentPercent = '0';

                    allMeeting.averageDuration = (Number(allMeeting.averageDuration) + Number(manager.averageDuration));
                    manager.averageDuration = (manager.averageDuration / manager.meetingNum).toFixed(0);

                    manager.peopleVoting = ((manager.peopleVoting / manager.people) * 100).toFixed(0).toString();
                    if (manager.peopleVoting == 'NaN') manager.peopleVoting = '0';

                    manager.uniquePeople = _.uniq(manager.uniquePeople);

                    manager.agendaDesign = ((manager.agendaDesign * 100)/manager.totalFeedback).toFixed(0).toString();
                    if (manager.agendaDesign == 'NaN') manager.agendaDesign = '0';
                    manager.meetingcontent = ((manager.meetingcontent * 100)/manager.totalFeedback).toFixed(0).toString();
                    if (manager.meetingcontent == 'NaN') manager.meetingcontent = '0';
                    manager.facilitator = ((manager.facilitator * 100)/manager.totalFeedback).toFixed(0).toString();
                    if (manager.facilitator == 'NaN') manager.facilitator = '0';
                    manager.achievingTargets = ((manager.achievingTargets * 100)/manager.totalFeedback).toFixed(0).toString();
                    if (manager.achievingTargets == 'NaN') manager.achievingTargets = '0';
                    manager.relevanceMeeting = ((manager.relevanceMeeting * 100)/manager.totalFeedback).toFixed(0).toString();
                    if (manager.relevanceMeeting == 'NaN') manager.relevanceMeeting = '0';
                    manager.teamwork = ((manager.teamwork * 100)/manager.totalFeedback).toFixed(0).toString();
                    if (manager.teamwork == 'NaN') manager.teamwork = '0';
                    manager.importanceMeeting = ((manager.importanceMeeting * 100)/manager.totalFeedback).toFixed(0).toString();
                    if (manager.importanceMeeting == 'NaN') manager.importanceMeeting = '0';


                    /* ************************************ SET MANAGER PIE ****************************************************/

                    const agendapieTot = manager.pie.agendaDesignPie.excellent + manager.pie.agendaDesignPie.good + manager.pie.agendaDesignPie.average + manager.pie.agendaDesignPie.poor + manager.pie.agendaDesignPie.waste;
                    if(agendapieTot > 0) {
                        manager.pie.agendaDesignPie.excellent = ((manager.pie.agendaDesignPie.excellent / agendapieTot) * 100).toFixed(2);
                        manager.pie.agendaDesignPie.good = ((manager.pie.agendaDesignPie.good / agendapieTot) * 100).toFixed(2);
                        manager.pie.agendaDesignPie.average = ((manager.pie.agendaDesignPie.average / agendapieTot) * 100).toFixed(2);
                        manager.pie.agendaDesignPie.poor = ((manager.pie.agendaDesignPie.poor / agendapieTot) * 100).toFixed(2);
                        manager.pie.agendaDesignPie.waste = ((manager.pie.agendaDesignPie.waste / agendapieTot) * 100).toFixed(2);
                    }

                    const facilitatorPieTot = manager.pie.facilitatorPie.excellent + manager.pie.facilitatorPie.good + manager.pie.facilitatorPie.average + manager.pie.facilitatorPie.poor + manager.pie.facilitatorPie.waste;
                    if(facilitatorPieTot > 0) {
                        manager.pie.facilitatorPie.excellent = ((manager.pie.facilitatorPie.excellent / facilitatorPieTot) * 100).toFixed(2);
                        manager.pie.facilitatorPie.good = ((manager.pie.facilitatorPie.good / facilitatorPieTot) * 100).toFixed(2);
                        manager.pie.facilitatorPie.average = ((manager.pie.facilitatorPie.average / facilitatorPieTot) * 100).toFixed(2);
                        manager.pie.facilitatorPie.poor = ((manager.pie.facilitatorPie.poor / facilitatorPieTot) * 100).toFixed(2);
                        manager.pie.facilitatorPie.waste = ((manager.pie.facilitatorPie.waste / facilitatorPieTot) * 100).toFixed(2);
                    }

                    const meetingcontentPieTot = manager.pie.meetingcontentPie.excellent + manager.pie.meetingcontentPie.good + manager.pie.meetingcontentPie.average + manager.pie.meetingcontentPie.poor + manager.pie.meetingcontentPie.waste;
                    if(meetingcontentPieTot > 0) {
                        manager.pie.meetingcontentPie.excellent = ((manager.pie.meetingcontentPie.excellent / meetingcontentPieTot) * 100).toFixed(2);
                        manager.pie.meetingcontentPie.good = ((manager.pie.meetingcontentPie.good / meetingcontentPieTot) * 100).toFixed(2);
                        manager.pie.meetingcontentPie.average = ((manager.pie.meetingcontentPie.average / meetingcontentPieTot) * 100).toFixed(2);
                        manager.pie.meetingcontentPie.poor = ((manager.pie.meetingcontentPie.poor / meetingcontentPieTot) * 100).toFixed(2);
                        manager.pie.meetingcontentPie.waste = ((manager.pie.meetingcontentPie.waste / meetingcontentPieTot) * 100).toFixed(2);
                    }

                    const achievingTargetsPieTot = manager.pie.achievingTargetsPie.excellent + manager.pie.achievingTargetsPie.good + manager.pie.achievingTargetsPie.average + manager.pie.achievingTargetsPie.poor + manager.pie.achievingTargetsPie.waste;
                    if(achievingTargetsPieTot > 0) {
                        manager.pie.achievingTargetsPie.excellent = ((manager.pie.achievingTargetsPie.excellent / achievingTargetsPieTot) * 100).toFixed(2);
                        manager.pie.achievingTargetsPie.good = ((manager.pie.achievingTargetsPie.good / achievingTargetsPieTot) * 100).toFixed(2);
                        manager.pie.achievingTargetsPie.average = ((manager.pie.achievingTargetsPie.average / achievingTargetsPieTot) * 100).toFixed(2);
                        manager.pie.achievingTargetsPie.poor = ((manager.pie.achievingTargetsPie.poor / achievingTargetsPieTot) * 100).toFixed(2);
                        manager.pie.achievingTargetsPie.waste = ((manager.pie.achievingTargetsPie.waste / achievingTargetsPieTot) * 100).toFixed(2);
                    }

                    const relevanceMeetingPieTot = manager.pie.relevanceMeetingPie.excellent + manager.pie.relevanceMeetingPie.good + manager.pie.relevanceMeetingPie.average + manager.pie.relevanceMeetingPie.poor + manager.pie.relevanceMeetingPie.waste;
                    if(relevanceMeetingPieTot > 0) {
                        manager.pie.relevanceMeetingPie.excellent = ((manager.pie.relevanceMeetingPie.excellent / relevanceMeetingPieTot) * 100).toFixed(2);
                        manager.pie.relevanceMeetingPie.good = ((manager.pie.relevanceMeetingPie.good / relevanceMeetingPieTot) * 100).toFixed(2);
                        manager.pie.relevanceMeetingPie.average = ((manager.pie.relevanceMeetingPie.average / relevanceMeetingPieTot) * 100).toFixed(2);
                        manager.pie.relevanceMeetingPie.poor = ((manager.pie.relevanceMeetingPie.poor / relevanceMeetingPieTot) * 100).toFixed(2);
                        manager.pie.relevanceMeetingPie.waste = ((manager.pie.relevanceMeetingPie.waste / relevanceMeetingPieTot) * 100).toFixed(2);
                    }

                    const teamworkPieTot = manager.pie.teamworkPie.excellent + manager.pie.teamworkPie.good + manager.pie.teamworkPie.average + manager.pie.teamworkPie.poor + manager.pie.teamworkPie.waste;
                    if(teamworkPieTot > 0) {
                        manager.pie.teamworkPie.excellent = ((manager.pie.teamworkPie.excellent / teamworkPieTot) * 100).toFixed(2);
                        manager.pie.teamworkPie.good = ((manager.pie.teamworkPie.good / teamworkPieTot) * 100).toFixed(2);
                        manager.pie.teamworkPie.average = ((manager.pie.teamworkPie.average / teamworkPieTot) * 100).toFixed(2);
                        manager.pie.teamworkPie.poor = ((manager.pie.teamworkPie.poor / teamworkPieTot) * 100).toFixed(2);
                        manager.pie.teamworkPie.waste = ((manager.pie.teamworkPie.waste / teamworkPieTot) * 100).toFixed(2);
                    }

                    const importanceMeetingPieTot = manager.pie.importanceMeetingPie.excellent + manager.pie.importanceMeetingPie.good + manager.pie.importanceMeetingPie.average + manager.pie.importanceMeetingPie.poor + manager.pie.importanceMeetingPie.waste;
                    if(importanceMeetingPieTot > 0) {
                        manager.pie.importanceMeetingPie.excellent = ((manager.pie.importanceMeetingPie.excellent / importanceMeetingPieTot) * 100).toFixed(2);
                        manager.pie.importanceMeetingPie.good = ((manager.pie.importanceMeetingPie.good / importanceMeetingPieTot) * 100).toFixed(2);
                        manager.pie.importanceMeetingPie.average = ((manager.pie.importanceMeetingPie.average / importanceMeetingPieTot) * 100).toFixed(2);
                        manager.pie.importanceMeetingPie.poor = ((manager.pie.importanceMeetingPie.poor / importanceMeetingPieTot) * 100).toFixed(2);
                        manager.pie.importanceMeetingPie.waste = ((manager.pie.importanceMeetingPie.waste / importanceMeetingPieTot) * 100).toFixed(2);
                    }

                    allMeeting.managers.push(manager);
                }
            }

            /* ************************************ SET ALL MEETING ****************************************************/
            allMeeting.averageDuration = (allMeeting.averageDuration / allMeeting.meetingNum).toFixed(0);

            const temp = Math.max(allMeeting.wastePercent, allMeeting.poorPercent, allMeeting.averagePercent, allMeeting.goodPercent, allMeeting.excellentPercent);
            if (temp == allMeeting.wastePercent) {
                allMeeting.averageVote = 'waste';
                allMeeting.maxRatingPercent = ((allMeeting.wastePercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            }
            if (temp == allMeeting.poorPercent) {
                allMeeting.averageVote = 'poor';
                allMeeting.maxRatingPercent = ((allMeeting.poorPercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            }
            if (temp == allMeeting.averagePercent) {
                allMeeting.averageVote = 'average';
                allMeeting.maxRatingPercent = ((allMeeting.averagePercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            }
            if (temp == allMeeting.goodPercent) {
                allMeeting.averageVote = 'good';
                allMeeting.maxRatingPercent = ((allMeeting.goodPercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            }
            if (temp == allMeeting.excellentPercent) {
                allMeeting.averageVote = 'excellent';
                allMeeting.maxRatingPercent = ((allMeeting.excellentPercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            }

            if (temp == 0) allMeeting.averageVote = '';
            if (allMeeting.maxRatingPercent == 'NaN') allMeeting.maxRatingPercent = '0';

            allMeeting.wastePercent = ((allMeeting.wastePercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            if (allMeeting.wastePercent == 'NaN') allMeeting.wastePercent = '0';
            allMeeting.poorPercent = ((allMeeting.poorPercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            if (allMeeting.poorPercent == 'NaN') allMeeting.poorPercent = '0';
            allMeeting.averagePercent = ((allMeeting.averagePercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            if (allMeeting.averagePercent == 'NaN') allMeeting.averagePercent = '0';
            allMeeting.goodPercent = ((allMeeting.goodPercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            if (allMeeting.goodPercent == 'NaN') allMeeting.goodPercent = '0';
            allMeeting.excellentPercent = ((allMeeting.excellentPercent / allMeeting.peopleVoting) * 100).toFixed(0).toString();
            if (allMeeting.excellentPercent == 'NaN') allMeeting.excellentPercent = '0';


            allMeeting.agendaDesign = ((allMeeting.agendaDesign * 100)/allMeeting.totalFeedback).toFixed(0).toString();
            if (allMeeting.agendaDesign == 'NaN') allMeeting.agendaDesign = '0';
            allMeeting.meetingcontent = ((allMeeting.meetingcontent * 100)/allMeeting.totalFeedback).toFixed(0).toString();
            if (allMeeting.meetingcontent == 'NaN') allMeeting.meetingcontent = '0';
            allMeeting.facilitator = ((allMeeting.facilitator * 100)/allMeeting.totalFeedback).toFixed(0).toString();
            if (allMeeting.facilitator == 'NaN') allMeeting.facilitator = '0';
            allMeeting.achievingTargets = ((allMeeting.achievingTargets * 100)/allMeeting.totalFeedback).toFixed(0).toString();
            if (allMeeting.achievingTargets == 'NaN') allMeeting.achievingTargets = '0';
            allMeeting.relevanceMeeting = ((allMeeting.relevanceMeeting * 100)/allMeeting.totalFeedback).toFixed(0).toString();
            if (allMeeting.relevanceMeeting == 'NaN') allMeeting.relevanceMeeting = '0';
            allMeeting.teamwork = ((allMeeting.teamwork * 100)/allMeeting.totalFeedback).toFixed(0).toString();
            if (allMeeting.teamwork == 'NaN') allMeeting.teamwork = '0';
            allMeeting.importanceMeeting = ((allMeeting.importanceMeeting * 100)/allMeeting.totalFeedback).toFixed(0).toString();
            if (allMeeting.importanceMeeting == 'NaN') allMeeting.importanceMeeting = '0';

                allMeeting.uniquePeople = _.uniq(allMeeting.uniquePeople);
                allMeeting.peopleVoting = ((allMeeting.peopleVoting / allMeeting.people) * 100).toFixed(0).toString();
            if (allMeeting.peopleVoting == 'NaN') allMeeting.peopleVoting = '0';

                /* ************************************ SET ALL MEETING PIE ****************************************************/

                const agendapieTot = allMeeting.pie.agendaDesignPie.excellent + allMeeting.pie.agendaDesignPie.good + allMeeting.pie.agendaDesignPie.average + allMeeting.pie.agendaDesignPie.poor + allMeeting.pie.agendaDesignPie.waste;
                if(agendapieTot > 0) {
                    allMeeting.pie.agendaDesignPie.excellent = ((allMeeting.pie.agendaDesignPie.excellent / agendapieTot) * 100).toFixed(2);
                    allMeeting.pie.agendaDesignPie.good = ((allMeeting.pie.agendaDesignPie.good / agendapieTot) * 100).toFixed(2);
                    allMeeting.pie.agendaDesignPie.average = ((allMeeting.pie.agendaDesignPie.average / agendapieTot) * 100).toFixed(2);
                    allMeeting.pie.agendaDesignPie.poor = ((allMeeting.pie.agendaDesignPie.poor / agendapieTot) * 100).toFixed(2);
                    allMeeting.pie.agendaDesignPie.waste = ((allMeeting.pie.agendaDesignPie.waste / agendapieTot) * 100).toFixed(2);
                }

                const facilitatorPieTot = allMeeting.pie.facilitatorPie.excellent + allMeeting.pie.facilitatorPie.good + allMeeting.pie.facilitatorPie.average + allMeeting.pie.facilitatorPie.poor + allMeeting.pie.facilitatorPie.waste;
                if(facilitatorPieTot > 0) {
                    allMeeting.pie.facilitatorPie.excellent = ((allMeeting.pie.facilitatorPie.excellent / facilitatorPieTot) * 100).toFixed(2);
                    allMeeting.pie.facilitatorPie.good = ((allMeeting.pie.facilitatorPie.good / facilitatorPieTot) * 100).toFixed(2);
                    allMeeting.pie.facilitatorPie.average = ((allMeeting.pie.facilitatorPie.average / facilitatorPieTot) * 100).toFixed(2);
                    allMeeting.pie.facilitatorPie.poor = ((allMeeting.pie.facilitatorPie.poor / facilitatorPieTot) * 100).toFixed(2);
                    allMeeting.pie.facilitatorPie.waste = ((allMeeting.pie.facilitatorPie.waste / facilitatorPieTot) * 100).toFixed(2);
                }

                const meetingcontentPieTot = allMeeting.pie.meetingcontentPie.excellent + allMeeting.pie.meetingcontentPie.good + allMeeting.pie.meetingcontentPie.average + allMeeting.pie.meetingcontentPie.poor + allMeeting.pie.meetingcontentPie.waste;
                if(meetingcontentPieTot > 0) {
                    allMeeting.pie.meetingcontentPie.excellent = ((allMeeting.pie.meetingcontentPie.excellent / meetingcontentPieTot) * 100).toFixed(2);
                    allMeeting.pie.meetingcontentPie.good = ((allMeeting.pie.meetingcontentPie.good / meetingcontentPieTot) * 100).toFixed(2);
                    allMeeting.pie.meetingcontentPie.average = ((allMeeting.pie.meetingcontentPie.average / meetingcontentPieTot) * 100).toFixed(2);
                    allMeeting.pie.meetingcontentPie.poor = ((allMeeting.pie.meetingcontentPie.poor / meetingcontentPieTot) * 100).toFixed(2);
                    allMeeting.pie.meetingcontentPie.waste = ((allMeeting.pie.meetingcontentPie.waste / meetingcontentPieTot) * 100).toFixed(2);
                }

                const achievingTargetsPieTot = allMeeting.pie.achievingTargetsPie.excellent + allMeeting.pie.achievingTargetsPie.good + allMeeting.pie.achievingTargetsPie.average + allMeeting.pie.achievingTargetsPie.poor + allMeeting.pie.achievingTargetsPie.waste;
                if(achievingTargetsPieTot > 0) {
                    allMeeting.pie.achievingTargetsPie.excellent = ((allMeeting.pie.achievingTargetsPie.excellent / achievingTargetsPieTot) * 100).toFixed(2);
                    allMeeting.pie.achievingTargetsPie.good = ((allMeeting.pie.achievingTargetsPie.good / achievingTargetsPieTot) * 100).toFixed(2);
                    allMeeting.pie.achievingTargetsPie.average = ((allMeeting.pie.achievingTargetsPie.average / achievingTargetsPieTot) * 100).toFixed(2);
                    allMeeting.pie.achievingTargetsPie.poor = ((allMeeting.pie.achievingTargetsPie.poor / achievingTargetsPieTot) * 100).toFixed(2);
                    allMeeting.pie.achievingTargetsPie.waste = ((allMeeting.pie.achievingTargetsPie.waste / achievingTargetsPieTot) * 100).toFixed(2);
                }

                const relevanceMeetingPieTot = allMeeting.pie.relevanceMeetingPie.excellent + allMeeting.pie.relevanceMeetingPie.good + allMeeting.pie.relevanceMeetingPie.average + allMeeting.pie.relevanceMeetingPie.poor + allMeeting.pie.relevanceMeetingPie.waste;
                if(relevanceMeetingPieTot > 0) {
                    allMeeting.pie.relevanceMeetingPie.excellent = ((allMeeting.pie.relevanceMeetingPie.excellent / relevanceMeetingPieTot) * 100).toFixed(2);
                    allMeeting.pie.relevanceMeetingPie.good = ((allMeeting.pie.relevanceMeetingPie.good / relevanceMeetingPieTot) * 100).toFixed(2);
                    allMeeting.pie.relevanceMeetingPie.average = ((allMeeting.pie.relevanceMeetingPie.average / relevanceMeetingPieTot) * 100).toFixed(2);
                    allMeeting.pie.relevanceMeetingPie.poor = ((allMeeting.pie.relevanceMeetingPie.poor / relevanceMeetingPieTot) * 100).toFixed(2);
                    allMeeting.pie.relevanceMeetingPie.waste = ((allMeeting.pie.relevanceMeetingPie.waste / relevanceMeetingPieTot) * 100).toFixed(2);
                }

                const teamworkPieTot = allMeeting.pie.teamworkPie.excellent + allMeeting.pie.teamworkPie.good + allMeeting.pie.teamworkPie.average + allMeeting.pie.teamworkPie.poor + allMeeting.pie.teamworkPie.waste;
                if(teamworkPieTot > 0) {
                    allMeeting.pie.teamworkPie.excellent = ((allMeeting.pie.teamworkPie.excellent / teamworkPieTot) * 100).toFixed(2);
                    allMeeting.pie.teamworkPie.good = ((allMeeting.pie.teamworkPie.good / teamworkPieTot) * 100).toFixed(2);
                    allMeeting.pie.teamworkPie.average = ((allMeeting.pie.teamworkPie.average / teamworkPieTot) * 100).toFixed(2);
                    allMeeting.pie.teamworkPie.poor = ((allMeeting.pie.teamworkPie.poor / teamworkPieTot) * 100).toFixed(2);
                    allMeeting.pie.teamworkPie.waste = ((allMeeting.pie.teamworkPie.waste / teamworkPieTot) * 100).toFixed(2);
                }

                const importanceMeetingPieTot = allMeeting.pie.importanceMeetingPie.excellent + allMeeting.pie.importanceMeetingPie.good + allMeeting.pie.importanceMeetingPie.average + allMeeting.pie.importanceMeetingPie.poor + allMeeting.pie.importanceMeetingPie.waste;
                if(importanceMeetingPieTot > 0) {
                    allMeeting.pie.importanceMeetingPie.excellent = ((allMeeting.pie.importanceMeetingPie.excellent / importanceMeetingPieTot) * 100).toFixed(2);
                    allMeeting.pie.importanceMeetingPie.good = ((allMeeting.pie.importanceMeetingPie.good / importanceMeetingPieTot) * 100).toFixed(2);
                    allMeeting.pie.importanceMeetingPie.average = ((allMeeting.pie.importanceMeetingPie.average / importanceMeetingPieTot) * 100).toFixed(2);
                    allMeeting.pie.importanceMeetingPie.poor = ((allMeeting.pie.importanceMeetingPie.poor / importanceMeetingPieTot) * 100).toFixed(2);
                    allMeeting.pie.importanceMeetingPie.waste = ((allMeeting.pie.importanceMeetingPie.waste / importanceMeetingPieTot) * 100).toFixed(2);
                }

                res.status(200).send(allMeeting);
           }
           else{
                res.status(200).send(allMeeting);
            }
        }).catch(err => {
        console.log("index.js~dashboard~get~error", err);
        return res.status(500).send(err.message)
    });
});

module.exports = router;

