/**
 * Created by marcellomorra on 19/04/18.
 */

const express = require('express');
const user = require('../models/model.user');
const router = new express.Router();


router.put('/notificationToken', (req, res) => {
    const notificationToken = req.body.notificationToken;
    const email = req.user.email;
    const filter = {
        "email": email,
    };
    const update = {
        $set: {
            "notificationToken": notificationToken,
        }
    };

    user.putNotificationToken(filter, update)
        .then(results => {
            res.status(200).send(results);
        }).catch(err => {
        console.log("index.js~putNotificationToken~error", err);
        return res.status(500).send(err.message);
    });
});


module.exports = router;


