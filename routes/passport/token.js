const jwt = require('jsonwebtoken');
const User = require('mongoose').model('User');
const config = require('../../config');
const TokenStrategy = require('passport-token-auth').Strategy;

/**
 *  The Auth Checker middleware function.
 */
module.exports = new TokenStrategy({}, (token, done) => {
  token = token ? token.replace(/^Bearer /g, '').trim() : '';
  return jwt.verify(token, config.jwtSecret, (err, decoded) => {
    if (err) {
      return done(null, false, err);
    }
    const userId = decoded.sub;
    return User.findById(userId, (userErr, user) => {
      if (userErr || !user) {
        return done(userErr);
      }
      done(null, user);
    });
  });
});
