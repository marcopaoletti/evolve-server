module.exports = {
	login: require('./local-login'),
	signup: require('./local-signup'),
	token: require('./token'),
	serializeUser: require('./serializeUser')
}