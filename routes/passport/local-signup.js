 const user = require('../../models/model.user.js');
 const PassportLocalStrategy = require('passport-local').Strategy;

 /**
  * Return the Passport Local Strategy object.
  */
 module.exports = new PassportLocalStrategy({
   usernameField: 'email',
   passwordField: 'password',
   session: false,
   passReqToCallback: true,
 }, (req, email, password, done) => {
   
     const userData = {
       email: email.trim(),
       password: password.trim(),
       name: req.body.name.trim(),
       role: req.body.role.toLowerCase().trim()
     };

     const newUser = new user(userData);
     newUser.save((err_user) => {
       if (err_user) {
         return done(err_user);
       }
       return done(null);
     });
 });