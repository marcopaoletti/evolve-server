const passport_token = require('passport').authenticate('token');
const express        = require('express');
const passport       = require('passport');
const route          = require('express').Router();
const auth           = require('./auth/index');
const permission     = require('permission');
const meetingRouter  = require('./routes.meeting');
const userRouter     = require('./routes.user');
const dashboardRouter= require('./routes.dashboard');

//Passport strategies

route.use('/v1/auth', auth);
route.use('/v1/meetings', passport_token, permission(['admin', 'super', 'user']), meetingRouter);
route.use('/v1/user', passport_token, permission(['admin', 'super', 'user']), userRouter);
route.use('/v1/dashboard', passport_token, permission(['admin']), dashboardRouter);


module.exports = route;
