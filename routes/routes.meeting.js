const express = require('express');
const meeting = require('../models/model.meeting');
const router = new express.Router();

router.get('/', (req, res) => {
    const date = req.query.date;
	const options = {
        "attendee":{"$elemMatch":{"email":req.user.email, "rated" : -1}},
        "end": {$lt: date},
        "status" : "CONFIRMED",
    };
	meeting.getByOption(options)
		.then(results => {
			res.status(200).send(results);
		}).catch(err => {
			console.log("index.js~get/option~error", err);
			return res.status(500).send(err.message)
		});
});

router.put('/', (req, res) => {

    const uid = req.body.uid;
	const vote = req.body.vote;
	const comment = req.body.comment;
	const ratingArea = req.body.ratingArea;

    const filter = {
        "uid": uid,
        "attendee": {"$elemMatch":{"email":req.user.email, "rated" : -1}}
    };
    const update = {
        $set: {
        	"attendee.$.rated": vote,
            "attendee.$.comment": comment,
            "attendee.$.rating_areas": ratingArea,
        }
	};

	if(update["$set"]["attendee.$.comment"] == null || update["$set"]["attendee.$.comment"] == ''){
    	delete update["$set"]["attendee.$.comment"];
	}
    meeting.putVote(filter, update)
        .then(results => {
            res.status(200).send(results);
        }).catch(err => {
        console.log("index.js~put~error", err);
        return res.status(500).send(err.message);
    });
});

module.exports = router;

