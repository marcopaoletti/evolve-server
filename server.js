const express = require('express');
const app     = require('./app');
const fs      = require('fs');
const routes  = require('./routes/routes.index');
const nconf   = require('nconf');
const config  = require('./config.json');
const passport = require('passport');
const passport_options = require('./routes/passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const compression = require('compression');
const pushNotification = require('./notification');

// Hierarchical node.js configuration with files, environment variables
nconf.argv().env().file({ file: './config.json' });
app.use(passport.initialize());

passport.use('local-signup', passport_options.signup);
passport.use('local-login', passport_options.login);
passport.use('token', passport_options.token);
passport.serializeUser(passport_options.serializeUser);

// security best practices: https://expressjs.com/en/advanced/best-practice-security.html
app.set('trust proxy', 'loopback');
app.use(helmet());
app.use(helmet.noCache()); // module for disabling client-side caching
app.use(compression({
  threshold: 1024,
}));
// CORS middleware
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));

app.use(require('./routes/routes.index.js'));

const models  = require('./models/model.index.js');

app.listen(3000, () => console.log('Evolve server listening on port 3000!'));
